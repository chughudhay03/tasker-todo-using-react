import React, { useState } from 'react';
import { Container } from '@mui/material';
import { Header } from './components/Header';
import { TaskForm } from './components/TaskForm';
import { TaskDashboard } from './components/TaskDashboard';
import { v4 as uuidv4 } from 'uuid';

const App = () => {
    const [tasks, setTasks] = useState([]);
    const [task, setTask] = useState('');
    const [taskPriority, setTaskPriority] = useState('');

    function clearTask() {
        setTask('');
        setTaskPriority('');
    }

    function handleToggleCompletion(taskId) {
        // const updateTasks = tasks.map(task => {
        //     if(task.id === taskId){
        //         return {
        //             ...task,
        //             isCompleted: !task.isCompleted
        //              priority: !task.priority  
        //         };
        //     }else{
        //         return task;
        //     }
        // });
        // setTask(updateTasks);

        setTasks(tasks.map(task => task.id === taskId ? {...task, isCompleted: !task.isCompleted} : task));
    }

    function submitTask(evt) {
        evt.preventDefault();
        if(task === '' && taskPriority === '') {
            alert("Blank Input and Priority is Not Allowed");
        } else if(task === '') {
            alert("Blank Input is Not Allowed");
        } else if(taskPriority === ''){
            alert("Blank Priority is Not Allowed");
        } else {
            setTasks(tasks.concat({id: uuidv4(), value: task, isCompleted: false, priority: taskPriority}));
            setTask('');
            setTaskPriority('');
        }
    }

    function handleDelete(taskId) {
        setTasks(tasks.filter(task => task.id !== taskId));
    }

    function handleSortByPriority(order) {
        let sortedTasks = [...tasks].sort((a, b) => {
            if(order === 'Ascen')
                return (a.priority - b.priority);
            else
                return (b.priority - a.priority);
        });
        setTasks(sortedTasks);
    }

    return (
        <div>
            <Header/>
            <Container maxWidth='lg' sx={{ marginTop: 4 }}>
                <TaskForm task={task} taskPriority={taskPriority} onTaskPriority={setTaskPriority} onChange={setTask} onClear={clearTask} onSubmit={submitTask}/>
                <TaskDashboard tasks={tasks} onSort={handleSortByPriority} onToggle={handleToggleCompletion} onDelete={handleDelete}/>
            </Container>
        </div>
    );
};

export default App;
