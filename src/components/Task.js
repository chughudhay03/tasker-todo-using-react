import { Button, Checkbox, Chip, ListItem, ListItemSecondaryAction, ListItemText } from "@mui/material";
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';
import React from "react";

export const Task = ({task, onToggle, onDelete}) => {
    const textDecorationStyle = task.isCompleted ? { textDecoration: "line-through" } : {};
    let chipCompleteElement;
    let chipPriorityElement;
    if (task.isCompleted) {
        chipCompleteElement = <Chip sx={{marginLeft: 1}} label="Completed" variant="outlined" color="success" />;
    } else {
        chipCompleteElement = <Chip sx={{marginLeft: 1}} label="Pending" variant="outlined" color="warning" />;
    }

    if (task.priority === 0) {
        chipPriorityElement = <Chip label="Low" color="success" />;
    } else if (task.priority === 1) {
        chipPriorityElement = <Chip label="Medium" color="warning" />;
    } else if (task.priority === 2) {
        chipPriorityElement = <Chip label="High" color="error" />;
    }
    return (
        <ListItem
            key={1}
            sx={{
                paddingBottom: 2,
                paddingTop: 2,
                borderBottom: "solid 1px #AAA",
            }}
            disablePadding
        >
            <Checkbox checked={task.isCompleted} onClick={()=>onToggle(task.id)}  />
            <ListItemText
                primary={task.value}
                sx={textDecorationStyle}
            />

            <ListItemSecondaryAction>
                {chipPriorityElement}
                {chipCompleteElement}
                <Button variant="contained" color="error" sx={{ marginLeft: 5 }} onClick={() => onDelete(task.id)}>
                    <DeleteForeverOutlinedIcon /> Delete
                </Button>
            </ListItemSecondaryAction>
        </ListItem>
    );
};
