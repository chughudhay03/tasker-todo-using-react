import { Box, Card, FormControl, Grid, InputLabel, MenuItem, Select } from "@mui/material";
import React from "react";
import { TaskList } from "./TaskList";

export const TaskDashboard = ({tasks, onToggle, onDelete, onSort}) => {
    function handleSort(evt) {
        onSort(evt.target.value);
    }
    return (
        <Grid container justifyContent="left" sx={{ marginTop: 4 }}>
            <Grid item xs={2}>
                <Box sx={{ minWidth: 120 }}>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Sort By Priority</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Sort By Priority"
                            onChange={handleSort}
                        >
                            <MenuItem value={''}>None</MenuItem>
                            <MenuItem value={'Ascen'}>Low to High</MenuItem>
                            <MenuItem value={'Decen'}>High to Low</MenuItem>
                        </Select>
                    </FormControl>
                </Box>
            </Grid>
            <Grid item xs={12}>
                <Card sx={{ padding: 1 }} raised={true}>
                    <TaskList tasks={tasks} onToggle={onToggle} onDelete={onDelete}/>
                </Card>
            </Grid>
        </Grid>
    );
};
