import { Box, Button, Card, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from "@mui/material";
import DataSaverOnOutlinedIcon from '@mui/icons-material/DataSaverOnOutlined';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import React from "react";

export const TaskForm = ({task, taskPriority, onChange, onClear, onSubmit, onTaskPriority}) => {
    function handleChange(evt) {
        onChange(evt.target.value);
    }

    function handleTaskPriority(evt) {
        onTaskPriority(evt.target.value);
    }

    return (
        <Grid container justifyContent="center">
            <Grid item xs={12}>
                <Card sx={{ padding: 4 }} raised={true}>
                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs={6}>
                            <form action="" onSubmit={onSubmit}>
                                <TextField 
                                    id="task-input" 
                                    label="Insert your task" 
                                    fullWidth
                                    value={task}
                                    onChange={handleChange} 
                                />
                            </form>
                        </Grid>
                        <Grid item xs={2}>
                            <Box sx={{ minWidth: 120 }}>
                                <FormControl fullWidth>
                                    <InputLabel id="demo-simple-select-label">Priority</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        label="Priority"
                                        value={taskPriority}
                                        onChange={handleTaskPriority}
                                    >
                                        <MenuItem value={0}>Low</MenuItem>
                                        <MenuItem value={1}>Medium</MenuItem>
                                        <MenuItem value={2}>High</MenuItem>
                                    </Select>
                                </FormControl>
                            </Box>
                        </Grid>
                        <Grid item xs={2}>
                            <Button 
                                variant="contained" 
                                color="primary" 
                                fullWidth
                                onClick={onSubmit}
                            >
                                <DataSaverOnOutlinedIcon /> Submit
                            </Button>
                        </Grid>
                        <Grid item xs={2}>
                            <Button 
                                variant="contained" 
                                color="secondary" 
                                fullWidth
                                onClick={onClear}
                            >
                                <DeleteOutlinedIcon /> Clear
                            </Button>
                        </Grid>
                    </Grid>
                </Card>
            </Grid>
        </Grid>
    );
};
