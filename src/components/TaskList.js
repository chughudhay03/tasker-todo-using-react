import { List } from "@mui/material";
import React from "react";
import { Task } from "./Task";

export const TaskList = ({tasks, onToggle, onDelete}) => {
  return (
        <List sx={{ padding: 1 }}>
             { tasks.map((task) => (<Task key={task.id} task={task} onToggle={onToggle} onDelete={onDelete} /> ))}
        </List>
    );
};
